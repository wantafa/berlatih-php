<?php

    // require('animal.php');
    require('frog.php');
    require('ape.php');

    
$sheep = new Animal("Jaguar");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>";


$frog = new Frog("buduk");

echo "Name : " . $frog->name . "<br>";
echo "Legs : " . $frog->legs . "<br>";
echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
$frog->jump();

$ape = new Ape("Ape");

echo "Name : " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
$ape->yell();